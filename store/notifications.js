export const state = () => ({
  showNotification: false,
  notificationState: 'success'
})

export const mutations = {
  toggle (state, newState) {
    state.showNotification = newState
  }
}
