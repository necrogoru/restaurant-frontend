import Vuex from 'vuex'

export const state = () => ({
  currentUser: null
})

export const mutations = {
  loadCurrentUser (state, currentUser) {
    state.currentUser = currentUser
  }
}
