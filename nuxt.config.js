module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Restaurant',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'FrontEnd for restaurant App' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Source+Code+Pro:400,700' },
      {rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.3.1/css/all.css", integrity: "sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU", crossorigin: "anonymous" },
      { rel: "stylesheet", href: "https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css" }
    ]
  },
  css: [
    '@/assets/scss/main.scss'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: {
    name: 'chasing-dots',
    color: '#ab4114',
    background: 'white',
    height: '4px'
  },
  /*
  ** Build configuration
  */
  modules: [
    ['nuxt-sass-resources-loader', '@assets/scss/main.scss'],
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/toast'
  ],

  // Router defaults
  router: {
    linkExactActiveClass: 'active'
  },

  // Axios base
  axios: {
    baseURL: 'http://localhost:3001/api/'
  },

  // Toast defaults
  toast: {
    position: 'top-right',
    duration: 3500
  },

  // TODO: Ajustar NUXT Auth
  // Authentication configuration
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {url: '/user/login', method: 'post', propertyName:    'token' },
          logout: false,
          user: {url: '/user/user', method: 'get', propertyName: 'data'},
        },
        tokenRequired: true,
        tokenType: 'Bearer'
      },
      facebook: {
        client_id: 'your facebook app id',
        userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=about,name,picture{url},email',
        scope: ['public_profile', 'email']
      },
      google: {
        client_id: 'your gcloud oauth app client id'
      },
    },
    redirect: {
      login: '/?login=1',
      logout: '/',
      user: '/profile',
      callback:'/'
    }
  },

  build: {
    /*
    ** Run ESLint on save
    */
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    },
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
